<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;

class Todo
{
  public static $todos = [
    ["task" => "Mengerjakan fitur get user", "photo" => null],
    ["task" => "Mengerjakan fitur create user", "photo" => null],
    ["task" => "Mengerjakan fitur edit user", "photo" => null],
    ["task" => "Mengerjakan fitur delete user", "photo" => null]
  ];

  public static function getAll()
  {
    $user = 'arya';
    return self::$todos;
  }

  public static function add($todo)
  {
    if (is_array($todo)) {
      // $filename = "evidence_".(string)$todo['photo'];
      // $path = Storage::putFileAs('todo', $todo['photo'],$filename );
      $todo['photo'] = null;
      self::$todos[] = $todo;
      return $todo;
    }
    return "Tipe data salah";
  }

  public static function delete($index)
  {
    if (isset(self::$todos[$index])) {
      unset(self::$todos[$index]);
      return true;
    }
    return false;
  }
}
