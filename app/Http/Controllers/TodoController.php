<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Todo;

class TodoController extends Controller
{
    public function index()
    {
        $todos = Todo::getAll();
        return response()->json($todos);
    }

    public function store(Request $request)
    {
        $request->validate([
            'task' => 'required|string',
            'photo' => 'required|file|mimes:jpeg,png,jpg,gif',
        ]);
        $true = true;

        $todo = [
            'task' => $request->task,
            'photo' => null
        ];

        Todo::add($todo);
        return response()->json('Todo added successfully', 201);
    }

    public function destroy($index)
    {
        if (Todo::delete($index)) {
            return response()->json('Todo deleted successfully');
        }
        return response()->json('Todo not found', 404);
    }
}
