<?php

namespace Tests\Unit\Model;

use Tests\TestCase;
use App\Models\Todo;

class TodoTest extends TestCase
{
    /**
     * A basic unit test example.
     */
    // public function test_mengambil_daftar_todo(): void
    // {
    //     $data = Todo::getAll();
    //     $this->assertIsArray($data);
    // }

    // public function test_membuat_todo_dan_memeriksa_apakah_tersimpan_pada_array_todo(): void
    // {
    //     // membuat dummy file
    //     $filename = 'dummy.txt';
    //     $file = fopen($filename, 'w');
    //     fwrite($file, 'Ini adalah konten dari file dummy');
    //     fclose($file);
    //     $todo = ["task"=>"main","photo"=>$filename];
    //     $result = Todo::add($todo);
    //     $todos = Todo::$todos;

    //     $expect_todos = ["task"=>"main","photo"=>"todo/evidence_dummy.txt"];
    //     $this->assertIsArray($todos);
    //     $file_exists = file_exists('storage/app/'.$result['photo']);
    //     $this->assertTrue($file_exists);
    //     $this->assertContains($expect_todos,$todos);
    // }

    // public function test_memasukan_tipe_data_selain_array_saat_membuat_todo(): void
    // {
    //     $todo = "main";
    //     $result = Todo::add($todo);
    //     $todos = Todo::$todos;
    //     $this->assertEquals("Tipe data salah",$result);
    //     $this->assertNotContains($todo,$todos);
    // }

    // public function test_menghapus_todo_dengan_index_yang_salah(): void
    // {
    //     $result = Todo::delete(-1);
    //     $this->assertFalse($result);
    // }

    // public function test_menghapus_todo(): void
    // {
    //     $todo = "nyuci baju";
    //     Todo::add($todo);
    //     $todos = Todo::$todos;
    //     $index = array_search($todo,$todos);
    //     $result = Todo::delete($index);

    //     $this->assertTrue($result);

    //     $todos = Todo::$todos;
    //     $this->assertNotContains($todo,$todos);
    // }

    public function test_menampilkan_data_todo_sebanyak_4_data(){
        $todo = new Todo();

        $result = $todo->getAll();

        $this->assertIsArray($result);

        $data_count = count($result);

        $expected_data_count = 4;

        $this->assertEquals($expected_data_count,$data_count);
    }

    public function test_menambahkan_task_dan_upload_photo_kedalam_todo()
    {
        $todo = new Todo();

        $param = [
            'task' => 'Membuat fitur manage role',
            'photo' => null,
        ];

        $filename = "manage_role.png";

        $file = fopen($filename,'w');
        fwrite($file,'test gambar png');
        fclose($file);

        $param['photo'] = $filename;

        $result = $todo->add($param);

        $this->assertIsArray($result);
        $this->assertArrayHasKey('photo',$result);
        $this->assertArrayHasKey('task',$result);

        $actual_task = $result['task'];
        $expected_task = 'Membuat fitur manage role';
        $this->assertEquals($expected_task,$actual_task);

        $actual_photo = 'storage/app/'.$result['photo'];

        $this->assertFileExists($actual_photo);
    }

    public function test_menambahkan_todo_dengan_parameter_string(){
        $todo = new Todo();
        $result = $todo->add("tes string");

        $expected_result = "Tipe data salah";
        $actual_result = $result;
        $this->assertEquals($expected_result, $actual_result);
    }

    public function test_menghapus_todo(){
        $result = Todo::delete(0);

        $this->assertTrue($result);
    }




}
